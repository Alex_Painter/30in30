# Lessons learned

 - if you want to calculate a percentage value for a non 0 based range (e.g 0.4 - 4), you can do:

 ```javascript
const value = percent * (max - min) + min
 ```
 - where `percent` is the decimal representation of the percentage (i.e. .56)