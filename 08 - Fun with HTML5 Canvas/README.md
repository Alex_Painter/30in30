# Lessons learned

 - use the HTML `<canvas>` element to provide an area to draw shapes
 - the actual drawing happens on the `context` which is retrieved like so:

```javascript
  const ctx = canvas.getCanvas('2d')
```

 - then, for example, to draw a line you need to set the current position, move to a new one, and render a stroke between the two points (path):

```javascript
  ctx.startPath()
  ctx.moveTo(oldX, oldY)
  ctx.lineTo(currentX, currentY)
  ctx.stroke()
```
