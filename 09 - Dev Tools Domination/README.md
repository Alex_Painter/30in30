## Lessons learned

 - `console.table` pretty prints objects and arrays
 - use `.warn` and `.error` to convey more meaning for the user
 - `console.assert` can be used for basic assertions
 - if you want to nicely print a DOM Element, use `console.dir`. Now you will be able to see all the attributes in a dropdown list
 - To quicky time an operation you can do `console.time('name')` and `console.timeEnd('name')`
