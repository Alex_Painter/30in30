## Lessons learned
- Use `console.table(arrayObject)` for a fancy print in Chrome dev tools
- Use `\`$[string}\` to use template strings`

- `Array.prototype.filter` method creates a new array containing all elements that pass the test in the provided function:

``` javascript
  const results = Array.prototype.filter(ele => {
    // if ele passes, return true
  })
```

- `Array.Prototype.map` returns a new array, after executing the callback function on each element in turn

``` javascript
  const results = Array.prototype.map(element => {
    //return value based on implemented logic
  })
```

- `Array.Prototype.sort` returns an array with element sorted acording to function provided. Function may not be necessarily stable, meaning if two elements are considered equal, original ordered may not be retained
- To return element `a` higher (i.e with a lower index), return below 0

``` javascript
  const results = Array.prototype.sort((a, b) => {
    // if a should be higher than b, return -1, otherwise return 1
  })
```

- `Array.Prototype.reduce` returns the accumulated value from each pass through the array
- Can pass in an initial value after the function definintion

``` javascript
  const results = Array.prototype.reduce((running, ele) => {
    //return a value
  }, 0 || {})
```
