## Lessons learned

- Use `fetch(endpoint)` to fetch resources, returns a promise that resolves to a `Response` object
- Then use `Response.json()` to retreive response data in json format

- `new RegExp(regexString, flags)` to create a regular expression object which can then be used in `String` methods such as `match` and `replace`

- To create HTML based on an array of data, map over the data a return a string of HTML
> make sure to use `.join('')` to return a complete string, rather than an array of strings
