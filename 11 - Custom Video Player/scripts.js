/* DOM elements */
const player = document.querySelector('.player')
const video = player.querySelector('.player__video')
const toggle = player.querySelector('.player__button')
const progress = player.querySelector('.progress')
const progressFill = player.querySelector('.progress__filled')
const ranges = Array.from(player.querySelectorAll('input.player__slider'))

/* Functions */
function onPlayToggle() {
	video.paused ? video.play() : video.pause()
}

function updateButton() {
	const icon = video.paused ? '►' : '||'
	toggle.textContent = icon
}

function updateRange() {
  video[this.name] = this.value
}

function updateProgress() {
  const percent = (video.currentTime / video.duration) * 100
  progressFill.style.flexBasis = `${percent}%`
}

function onSeek(e) {
  const percent = (e.offsetX / progress.offsetWidth)
  video.currentTime = video.duration * percent
}

/* Event listeners */
video.addEventListener('click', onPlayToggle)
video.addEventListener('play', updateButton)
video.addEventListener('pause', updateButton)
video.addEventListener('timeupdate', updateProgress)
video.addEventListener('load', updateProgress)

toggle.addEventListener('click', onPlayToggle)

ranges.forEach(range => {
	range.addEventListener('input', updateRange)
})

progress.addEventListener('click', onSeek)



