## Lessons learned

 - use `localStorage.setItem('item', JSON.stringify(items))` to put a string representation of an object into local storage
 - use `<element>.matches('tag')` to check element type
 - when adding items to a list, use something like `data-index=${index}` to give it an unique index, then to access you can do `ele.data.index` to get the value