## Lessons learned

 - use `Array.some()` to check that at least one in the array meets the given requirement(s)
 - use `Array.every()` to check that every item in the array meets the ginen requirement(s)
 - `Array.find()` works similar to `.filter()` however it only ever returns one item that matches
 - when removing an item from an array, you can use `Array.index` to find the position of the item, then use a variation of `splice()` to manipulate the array:

```javascript
  const singleSplice = arr.splice(index)

  // or

  const doubleSplice = [
    ...arr.splice(0, index)
    ...arr.splice(index)
  ]
```
