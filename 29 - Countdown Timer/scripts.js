let timeInterval
timerDisplay = document.querySelector('.display__time-left')
endTimeDisplay = document.querySelector('.display__end-time')
form = document.querySelector('#custom')
buttons = document.querySelectorAll('.timer__controls button')

function timer(seconds) {
	clearInterval(timeInterval)

	const now = Date.now()
	const then = now + seconds * 1000
	displayTime(seconds)
	displayEndTime(then)

	timeInterval = setInterval(() => {
		const secondsLeft = Math.round((then - Date.now()) / 1000)
		if (secondsLeft < 0) {
			return
		}

		displayTime(secondsLeft)
	}, 1000)
}

function displayTime(seconds) {
	const minutes = Math.floor(seconds / 60)
	const remainingSeconds = seconds % 60
	timerDisplay.textContent = `${minutes}:${pad(remainingSeconds)}`
}

function displayEndTime(milliSeconds) {
	const endDate = new Date(milliSeconds)
	const endMinutes = endDate.getMinutes()
	const endHour = endDate.getHours()
	endTimeDisplay.textContent = `Return at ${pad(endHour)}:${pad(endMinutes)}`
}

function pad(time) {
	return time < 10 ? `0${time}` : time
}

function setTimer() {
	timer(parseInt(this.dataset.time))
}

buttons.forEach(button => button.addEventListener('click', setTimer))
form.addEventListener('submit', function(e) {
	e.preventDefault()
	const seconds = parseInt(this.minutes.value) * 60
	timer(seconds)
})

