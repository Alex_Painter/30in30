## Lessons learned

- `display: flex` defines a flex box container; from here you can just `justify-content` and `align-items` to control the layout of it's content
- `flex: n` for the flex items defines how much space to take up compared to it's siblings 

- `.panel > div` will select all immediate children of type div
- `.panel *:nth-child` for selecting specific children

[Flex guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
