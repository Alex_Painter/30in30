## Lessons learned

 - Dynamically set an element's shadow offset by using event x and y offsets
 - Object destructuring: `const { objectProperty: variable, secondProperty: variable2 } = object`