## Lessons learned

 - when making changes to items in an array, you don't necessarily need to know the index of other items, you can just do:

```
  // inside a forEach in a event handler function
  if (item === this)
    // flip a flag to change functionality
```

 - the `click` event comes with a handy `shiftKey` flag!

