# Lessons learned

 - to stop events being propagated to parent elements use `e.stopPropagation()`
 - you can also set the function to fire on the event capture, not the event propagation by using:

```javascript
	ele.addEventListener('event', function, {
		capture: true
		})

	// this means the event will be fired when the browsers is figuring out what has been clicked, when moving down the hierachy, rather than when in propagates back up
```

 - we can also only allow the bound event function to fire once, by using:

```javascript
	ele.addEventListener('event', function, {
		once: true
		})

	// this will unbind the function after the first time it's been fired 
```
