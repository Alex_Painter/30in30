## Lessons learned

- CSS fiters: `filter: blur(10px);`

- Dataset is all the 'data-' objects attached to the element

- Use CSS variables like:

``` css
root: {
  --spacing: 10px;
}

.button {
  padding: var(--spacing);
}
```
- Update CSS variables through JS by using `document.documentElement.style.setProperty`
