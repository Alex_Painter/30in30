# Lessons learned

 - when you have UI controls that will change the values on an object, make the `name` the same as the property on the object, so when you update you can do `object[this.name] = this.value`